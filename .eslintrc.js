module.exports = {
  extends: ['eslint:recommended', 'plugin:vue/base'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    'no-console': 0,
    'require-jsdoc': 1,
    'valid-jsdoc': 1
  }
}
